<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_id',
        'id_documents',
        'is_returns',
        'tanggal_returns',
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'id_documents');
    }
}
