<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'nama',
        'waktu',
    ];

    
    public function transactionDetails()
    {
        return $this->hasMany(TransactionDetail::class);
    }

}
