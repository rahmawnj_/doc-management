<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    use HasFactory;

    protected $fillable = ['rak_id', 'nama_kardus'];

    public function rak()
    {
        return $this->belongsTo(Rak::class);
    }
}
