<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    use HasFactory;
    
    protected $fillable = ['box_id', 'nama_map'];

    public function box()
    {
        return $this->belongsTo(Box::class);
    }
}
