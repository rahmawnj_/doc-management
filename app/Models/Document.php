<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama',
        'nomor',
        'date_document',
        'qrcode',
        'uraian',
        'spk',
        'date_spk',
        'rab',
        'bap',
        'bastb',
        'sp2d',
        'date_sp2d',
        'spm',
        'spj',
        'nilai',
        'pt',
        'completed',
        'id_ruangan',
        'id_rak',
        'id_box',
        'id_map',
        'id_category',
        'id_bidang',
        'id_user',
        'id_skpd',
    ];

    public function ruangan()
    {
        return $this->belongsTo(Ruangan::class, 'id_ruangan');
    }

    public function rak()
    {
        return $this->belongsTo(Rak::class, 'id_rak');
    }

    public function box()
    {
        return $this->belongsTo(Box::class, 'id_box');
    }

    public function map()
    {
        return $this->belongsTo(Map::class, 'id_map');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'id_category');
    }

    public function bidang()
    {
        return $this->belongsTo(Bidang::class, 'id_bidang');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function skpd()
    {
        return $this->belongsTo(Skpd::class, 'id_skpd');
    }
}
