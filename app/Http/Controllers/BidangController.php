<?php

namespace App\Http\Controllers;

use App\Models\Bidang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BidangController extends Controller
{
    public function index()
    {
        $bidangs = Bidang::all();
        return view('be.bidangs.index', compact('bidangs'));
    }

    public function create()
    {
        return view('be.bidangs.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:bidangs|max:255',
        ]);

        Bidang::create($validatedData);

        return redirect('/bidangs')->with('success', 'Bidang berhasil dibuat.');
    }

    public function edit(Bidang $bidang)
    {
        return view('be.bidangs.edit', compact('bidang'));
    }

    public function update(Request $request, Bidang $bidang)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
        ]);

        $bidang->update($validatedData);

        return redirect('/bidangs')->with('success', 'Bidang berhasil diperbarui.');
    }

    public function destroy(Bidang $bidang)
    {
        $bidang->delete();

        return redirect('/bidangs')->with('success', 'Bidang berhasil dihapus.');
    }
}
