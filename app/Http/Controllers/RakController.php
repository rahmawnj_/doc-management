<?php

namespace App\Http\Controllers;

use App\Models\Rak;
use Illuminate\Http\Request;

class RakController extends Controller
{
    public function index()
    {
        $raks = Rak::all();
        return view('be.raks.index', compact('raks'));
    }

    public function create()
    {
        return view('be.raks.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kode_rak' => 'required|unique:raks',
        ]);

        Rak::create($validatedData);

        return redirect('/raks')->with('success', 'Rak berhasil dibuat.');
    }

    public function edit(Rak $rak)
    {

        return view('be.raks.edit', compact('rak'));
    }

    public function update(Request $request, Rak $rak)
    {
        $validatedData = $request->validate([
            'kode_rak' => 'required|unique:raks,kode_rak,' . $rak->id,
        ]);

        $rak->update($validatedData);

        return redirect('/raks')->with('success', 'Rak berhasil diperbarui.');
    }

    public function destroy(Rak $rak)
    {
        $rak->delete();

        return redirect('/raks')->with('success', 'Rak berhasil dihapus.');
    }
}
