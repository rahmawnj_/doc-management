<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\TransactionDetail;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function pinjam()
    {
        return view('be.transaksi.pinjam');
    }
    public function kembali()
    {
        $transactionDetails = TransactionDetail::where('is_returns', 0)->get();

        return view('be.transaksi.kembali', compact('transactionDetails'));
    }



    public function submitDocuments(Request $request)
    {
        $qrCodes = $request->input('qrcode');

        // Membuat record Transaction baru
        $transaction = Transaction::create([
            'nama' => $request->inputNama,
            'waktu' => now(),
        ]);

        // Memasukkan data ke dalam tabel transaction_details
        foreach ($qrCodes as $qrCode) {
            $document = Document::where('qrcode', $qrCode)->first();
            if ($document) {
                TransactionDetail::create([
                    'transaction_id' => $transaction->id,
                    'id_documents' => $document->id,
                    'is_returns' => false, // Ini merupakan peminjaman
                    'tanggal_returns' => null, // Tanggal pengembalian dapat diatur jika diperlukan
                ]);
            }
        }

        // Menandai dokumen sebagai completed
        Document::whereIn('qrcode', $qrCodes)->update(['completed' => 1]);

        return redirect()->route('pinjam')->with('success', 'Dokumen Berhasil Dipinjam.');

    }

    public function returnDocuments(Request $request)
    {
        $qrCode = $request->input('qrcode');

        // Find the document by QRCode
        $document = Document::where('qrcode', $qrCode)->first();

        if ($document) {
            // Update the document as returned
            $document->update([
                'completed' => 0,
            ]);

            // Find the most recent corresponding TransactionDetail record and update it
            $transactionDetail = TransactionDetail::where('id_documents', $document->id)
                ->where('is_returns', false) // Only update if it's a loan, not a return
                ->latest() // Get the most recent record
                ->first();

            if ($transactionDetail) {
                $transactionDetail->update([
                    'is_returns' => true,
                    'tanggal_returns' => now(), // Set the return date
                ]);
            }

            return redirect()->back()->with('success', 'Document returned successfully.');
        } else {
            return redirect()->back()->with('error', 'QRCode not found.');
        }
    }

    public function index()
    {
        $transactions = Transaction::orderBy('id', 'desc')->get();
        return view('be.transaksi.index', compact('transactions'));
    }
}
