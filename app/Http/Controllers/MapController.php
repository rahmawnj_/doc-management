<?php

namespace App\Http\Controllers;

use App\Models\Map;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapController extends Controller
{
    public function index()
    {
        $maps = Map::all();
        return view('be.maps.index', compact('maps'));
    }

    public function create()
    {
        return view('be.maps.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_map' => 'required|unique:maps|max:255',
        ]);

        Map::create($validatedData);

        return redirect('/maps')->with('success', 'Map berhasil dibuat.');
    }

    public function show(Map $map)
    {
        return view('be.maps.show', compact('map'));
    }

    public function edit(Map $map)
    {
        return view('be.maps.edit', compact('map'));
    }

    public function update(Request $request, Map $map)
    {
        $validatedData = $request->validate([
            'nama_map' => 'required|max:255',
        ]);

        $map->update($validatedData);

        return redirect('/maps')->with('success', 'Map berhasil diperbarui.');
    }

    public function destroy(Map $map)
    {
        $map->delete();

        return redirect('/maps')->with('success', 'Map berhasil dihapus.');
    }
}
