<?php

namespace App\Http\Controllers;

use App\Models\skpd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class skpdController extends Controller
{
    public function index()
    {
        $skpds = skpd::all();
        return view('be.skpds.index', compact('skpds'));
    }

    public function create()
    {
        return view('be.skpds.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:skpds|max:255',
        ]);

        skpd::create($validatedData);

        return redirect('/skpds')->with('success', 'skpd berhasil dibuat.');
    }

    public function edit(skpd $skpd)
    {
        return view('be.skpds.edit', compact('skpd'));
    }

    public function update(Request $request, skpd $skpd)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
        ]);

        $skpd->update($validatedData);

        return redirect('/skpds')->with('success', 'skpd berhasil diperbarui.');
    }

    public function destroy(skpd $skpd)
    {
        $skpd->delete();

        return redirect('/skpds')->with('success', 'skpd berhasil dihapus.');
    }
}
