<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('be.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('be.categories.create');
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'nama' => 'required|unique:categories|max:255',
        ]);

        Category::create($validatedData);

        return redirect('/categories')->with('success', 'Category berhasil dibuat.');
    }

    public function show(Category $category)
    {
        return view('be.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('be.categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
        ]);

        $category->update($validatedData);

        return redirect('/categories')->with('success', 'Category berhasil diperbarui.');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect('/categories')->with('success', 'Category berhasil dihapus.');
    }
}
