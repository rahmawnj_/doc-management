<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\Map;
use App\Models\Rak;
use App\Models\Skpd;
use App\Models\User;
use App\Models\Bidang;
use App\Models\Ruangan;
use App\Models\Category;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{
    public function index()
    {
        $documents = Document::all();
        return view('be.documents.index', compact('documents'));
    }

    public function create()
    {
        $ruangans = Ruangan::all();
        $raks = Rak::all();
        $boxes = Box::all();
        $maps = Map::all();
        $categories = Category::all();
        $bidangs = Bidang::all();
        $users = User::all();
        $skpds = Skpd::all();

        return view('be.documents.create', compact('ruangans', 'raks', 'boxes', 'maps', 'categories', 'bidangs', 'users', 'skpds'));
    }


    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'nomor' => 'required|unique:documents',
            'date_document' => 'required|date',
            'qrcode' => 'nullable',
            'uraian' => 'nullable',
            'spk' => 'nullable',
            'date_spk' => 'nullable|date',
            'rab' => 'nullable',
            'bap' => 'nullable',
            'bastb' => 'nullable',
            'sp2d' => 'nullable',
            'date_sp2d' => 'nullable|date',
            'spm' => 'nullable',
            'spj' => 'nullable',
            'nilai' => 'nullable|numeric',
            'pt' => 'nullable',
            // 'completed' => 'boolean',
            'id_ruangan' => 'nullable|exists:ruangans,id',
            'id_rak' => 'nullable|exists:raks,id',
            'id_box' => 'nullable|exists:boxes,id',
            'id_map' => 'nullable|exists:maps,id',
            'id_category' => 'nullable|exists:categories,id',
            'id_bidang' => 'nullable|exists:bidangs,id',
            'id_user' => 'nullable|exists:users,id',
            'id_skpd' => 'nullable|exists:skpds,id',
        ]);


        Document::create($validatedData);

        return redirect('/documents')->with('success', 'Document berhasil dibuat.');
    }

    public function show(Document $document)
    {
        return view('be.documents.show', compact('document'));
    }

    public function edit(Document $document)
    {
        $ruangans = Ruangan::all();
        $raks = Rak::all();
        $boxes = Box::all();
        $maps = Map::all();
        $categories = Category::all();
        $bidangs = Bidang::all();
        $users = User::all();
        $skpds = Skpd::all();
        return view('be.documents.edit', compact('document', 'ruangans', 'raks', 'boxes', 'maps', 'categories', 'bidangs', 'users', 'skpds'));
    }

    public function update(Request $request, Document $document)
    {

        $validatedData = $request->validate([
            'nama' => 'required',
            'nomor' => 'required|unique:documents,nomor,' . $document->id,
            'date_document' => 'required|date',
            'qrcode' => 'nullable',
            'uraian' => 'nullable',
            'spk' => 'nullable',
            'date_spk' => 'nullable|date',
            'rab' => 'nullable',
            'bap' => 'nullable',
            'bastb' => 'nullable',
            'sp2d' => 'nullable',
            'date_sp2d' => 'nullable|date',
            'spm' => 'nullable',
            'spj' => 'nullable',
            'nilai' => 'nullable|numeric',
            'pt' => 'nullable',
            // 'completed' => 'boolean',
            'id_ruangan' => 'nullable|exists:ruangans,id',
            'id_rak' => 'nullable|exists:raks,id',
            'id_box' => 'nullable|exists:boxes,id',
            'id_map' => 'nullable|exists:maps,id',
            'id_category' => 'nullable|exists:categories,id',
            'id_bidang' => 'nullable|exists:bidangs,id',
            'id_user' => 'nullable|exists:users,id',
            'id_skpd' => 'nullable|exists:skpds,id',
        ]);

        $document->update($validatedData);

        return redirect('/documents')->with('success', 'Document berhasil diperbarui.');
    }

    public function destroy(Document $document)
    {

        $document->delete();

        return redirect('/documents')->with('success', 'Document berhasil dihapus.');
    }

    public function updateCompleted(Document $document)
    {
        $document->update([
            'completed' => request('completed') ? true : false,
        ]);
        return redirect()->route('documents.index')->with('success', 'Document berhasil dihapus.');
    }

    public function status($id)
    {

        $document = Document::find($id);

        if (!$document) {
            return response()->json(['error' => 'Dokumen tidak ditemukan'], 404);
        }

        return response()->json(['completed' => $document->completed]);
    }

    public function status_document(Request $request, $id)
    {
        // Cek apakah id ruangan valid
        $ruangan = Ruangan::find($id);

        if (!$ruangan) {
            return response()->json(['message' => 'ID Ruangan tidak valid'], 404);
        }

        // Ambil data QR code dari permintaan
        $qrcodes = json_decode($request->getContent(), true);

        if (!is_array($qrcodes)) {
            return response()->json(['message' => 'Data QR code tidak valid'], 400);
        }

        // Cek apakah ada QR code di tabel documents
        $documents = Document::whereIn('qrcode', $qrcodes)->get();

        $allowedQRCodes = [];
        $notAllowedQRCodes = [];

        foreach ($documents as $document) {
            if ($document->id_ruangan == $id) {
                if ($document->completed == 0) {
                    // Trigger alarm karena ada dokumen yang belum diizinkan
                    // Di sini Anda dapat mengirim sinyal atau tindakan lain yang diperlukan
                    return response()->json([
                        'message' => 'Ada dokumen yang belum diizinkan untuk dipinjam',
                        'alarm_status' => 'alarm',
                    ], 403);
                } else {
                    $allowedQRCodes[] = $document->qrcode;
                }
            } else {
                $notAllowedQRCodes[] = $document->qrcode;
            }
        }

        if (empty($notAllowedQRCodes)) {
            return response()->json(['message' => 'Semua QR code yang diberikan ditemukan di ruangan ini', 'alarm_status' => 'normal'], 200);
        } else {
            return response()->json([
                'message' => 'Beberapa QR code tidak sesuai dengan ruangan',
                'not_allowed_qrcodes' => $notAllowedQRCodes,
                'alarm_status' => 'normal',
            ], 200);
        }
    }


    public function code_document(Request $request)
    {
        $qrcode = $request->input('value');

        // Cari dokumen berdasarkan qrcode
        $document = Document::where('qrcode', $qrcode)->first();

        if ($document) {
            // Jika ditemukan, tampilkan hasil ke dalam tabel
            return response()->json(['document' => $document]);
        } else {
            // Jika tidak ditemukan, kirim pesan "qrcode tidak ditemukan"
            return response()->json(['message' => 'qrcode tidak ditemukan']);
        }
    }

    public function showCheckDocumentForm(Request $request)
    {
        $qrCode = $request->input('qrCode');
        $document = Document::where('qrcode', $qrCode)->first();

        if ($document) {
            // Dokumen ditemukan
            return view('be.check', compact('document'));
        } else {
            // Dokumen tidak ditemukan, atur pesan kesalahan
            $message = 'Dokumen tidak ditemukan';
            return view('be.check', compact('message'));
        }
    }
}
