<?php

namespace App\Http\Controllers;

use App\Models\Ruangan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RuanganController extends Controller
{
    public function index()
    {
        $ruangans = Ruangan::all();
        return view('be.ruangans.index', compact('ruangans'));
    }

    public function create()
    {
        return view('be.ruangans.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|unique:ruangans|max:255',
        ]);

        Ruangan::create($validatedData);

        return redirect('/ruangans')->with('success', 'Ruangan berhasil dibuat.');
    }

    public function show(Ruangan $ruangan)
    {
        return view('be.ruangans.show', compact('ruangan'));
    }

    public function edit(Ruangan $ruangan)
    {
        return view('be.ruangans.edit', compact('ruangan'));
    }

    public function update(Request $request, Ruangan $ruangan)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
        ]);

        $ruangan->update($validatedData);

        return redirect('/ruangans')->with('success', 'Ruangan berhasil diperbarui.');
    }

    public function destroy(Ruangan $ruangan)
    {
        $ruangan->delete();

        return redirect('/ruangans')->with('success', 'Ruangan berhasil dihapus.');
    }
}
