<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\Rak;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function index()
    {
        // $boxes = Box::with('rak.ruangan')->get();
        $boxes = Box::all();
        return view('be.boxes.index', compact('boxes'));
    }

    public function create()
    {
        // $raks = Rak::all();
        return view('be.boxes.create',);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            // 'rak_id' => 'required',
            'nama_kardus' => 'required|unique:boxes',
        ]);
        Box::create($validatedData);
        return redirect('/boxes')->with('success', 'Box berhasil dibuat.');
    }

    public function edit(Box $box)
    {
        // $raks = Rak::all();
        return view('be.boxes.edit', compact('box'));
    }

    public function update(Request $request, Box $box)
    {
        $validatedData = $request->validate([
            // 'rak_id' => 'required',
            'nama_kardus' => 'required|unique:boxes,nama_kardus,' . $box->id,
        ]);

        $box->update($validatedData);
        return redirect('/boxes')->with('success', 'Box berhasil diperbarui.');
    }

    public function destroy(Box $box)
    {
        $box->delete();

        return redirect('/boxes')->with('success', 'Box berhasil dihapus.');
    }
}
