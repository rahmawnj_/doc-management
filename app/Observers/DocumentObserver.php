<?php

namespace App\Observers;

use App\Models\Document;
use App\Models\DocumentLog;
use Illuminate\Support\Facades\Auth;

class DocumentObserver
{
    public function created(Document $document)
    {
        $this->logChange($document, 'created');
    }

    public function updated(Document $document)
    {
        $this->logChange($document, 'updated', [
            'from' => $document->getOriginal('completed'),
            'to' => $document->completed,
        ]);
    }

    protected function logChange(Document $document, $action, $extraData = [])
    {
        $log = new DocumentLog();
        $log->document_id = $document->id;
        $log->user_id = Auth::user()->id; // Anda dapat menambahkan informasi pengguna yang melakukan perubahan
        $log->action = $action;
        $log->description = $this->getDescription($action, $extraData);
        $log->save();
    }

    protected function getDescription($action, $extraData)
    {
        switch ($action) {
            case 'created':
                return 'Dokumen dibuat.';
            case 'completed_status_changed':
                return 'Status completed diubah dari ' . $extraData['from'] . ' menjadi ' . $extraData['to'];
            case 'deleted':
                return 'Dokumen dihapus.';
            default:
                return 'Aksi tidak dikenali.';
        }
    }
}
