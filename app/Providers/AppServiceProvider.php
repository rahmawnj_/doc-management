<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\DocumentObserver;
use App\Models\Document;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }


    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Document::observe(DocumentObserver::class);
       
    }
}
