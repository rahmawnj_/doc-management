<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('nomor');
            $table->date('date_document');
            $table->string('qrcode')->nullable();
            $table->text('uraian')->nullable();
            $table->string('spk')->nullable();
            $table->date('date_spk')->nullable();
            $table->string('rab')->nullable();
            $table->string('bap')->nullable();
            $table->string('bastb')->nullable();
            $table->string('sp2d')->nullable();
            $table->date('date_sp2d')->nullable();
            $table->string('spm')->nullable();
            $table->string('spj')->nullable();
            $table->decimal('nilai', 15, 2)->nullable();
            $table->string('pt')->nullable();
            $table->boolean('completed')->default(false);
            $table->unsignedBigInteger('id_ruangan')->nullable();
            $table->unsignedBigInteger('id_rak')->nullable();
            $table->unsignedBigInteger('id_box')->nullable();
            $table->unsignedBigInteger('id_map')->nullable();
            $table->unsignedBigInteger('id_category')->nullable();
            $table->unsignedBigInteger('id_bidang')->nullable();
            $table->unsignedBigInteger('id_user')->nullable();
            $table->unsignedBigInteger('id_skpd')->nullable();
            $table->timestamps();

            $table->foreign('id_ruangan')->references('id')->on('ruangans')->onDelete('cascade');
            $table->foreign('id_rak')->references('id')->on('raks')->onDelete('cascade');
            $table->foreign('id_box')->references('id')->on('boxes')->onDelete('cascade');
            $table->foreign('id_map')->references('id')->on('maps')->onDelete('cascade');
            $table->foreign('id_category')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('id_bidang')->references('id')->on('bidangs')->onDelete('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_skpd')->references('id')->on('skpds')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
