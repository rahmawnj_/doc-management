<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        
            $table->unsignedBigInteger('transaction_id'); // Kolom foreign key ke tabel transactions
            $table->foreign('transaction_id')->references('id')->on('transactions');
        
            $table->unsignedBigInteger('id_documents'); // Kolom foreign key ke tabel documents
            $table->foreign('id_documents')->references('id')->on('documents');
        
            $table->boolean('is_returns')->default(false); // Kolom boolean untuk menandai apakah pengembalian atau peminjaman
            $table->date('tanggal_returns')->nullable(); // Kolom tanggal pengembalian (nullable, jika belum dikembalikan)
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaction_details');
    }
};
