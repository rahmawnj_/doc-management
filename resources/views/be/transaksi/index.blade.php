@extends('layouts.backend.app')

@php
    $title = 'Data Peminjaman';
@endphp

@push('styles')
    <link href="{{ asset('assets/be/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/be/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}"
        rel="stylesheet" />
    <link href="{{ asset('assets/be/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}"
        rel="stylesheet" />
@endpush
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    <h1 class="page-header">{{ $title ?? '' }}</h1>
    <!-- END page-header -->
    <!-- BEGIN panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <table id="table" class="table table-striped table-bordered align-middle">
                <thead>
                    <tr>
                        <th width="1%">No.</th>
                        <th class="text-nowrap">Nama</th>
                        <th class="text-nowrap">Total Document</th>
                        <th class="text-nowrap">Tanggal</th>
                        <th class="text-nowrap"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $transaction->nama }}</td>
                            <td>{{ $transaction->transactionDetails()->count() }}</td>
                            <td>{{ $transaction->waktu }}</td>
                            <td>
                                <!-- Tombol untuk membuka modal -->
                                <a href="#detailModal{{ $transaction->id }}" class="btn btn-primary"
                                    data-bs-toggle="modal">Detail</a>

                                <!-- Modal -->
                                <div class="modal fade" id="detailModal{{ $transaction->id }}" tabindex="-1"
                                    aria-labelledby="detailModal{{ $transaction->id }}Label" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="detailModal{{ $transaction->id }}Label">Detail
                                                    Transaction</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Nama Peminjam : {{ $transaction->nama }}</p>
                                                <p>Tanggal Pinjam : {{ $transaction->waktu }}</p>
                                                <div class="modal-body">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Transaction Name</th>
                                                                <th>Status</th>
                                                                <th>Ruangan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($transaction->transactionDetails as $item)
                                                                <tr>
                                                                    <td>{{ $item->document->nama }}</td>
                                                                    <td>
                                                                        @if ($item->is_returns == 1)
                                                                            <span
                                                                                class="badge bg-success">Sudah
                                                                                Dikembalikan</span>
                                                                        @else
                                                                            <span
                                                                                class="badge bg-danger">Belum
                                                                                Dikembalikan</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ $item->document->ruangan->nama }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <!-- Anda dapat menambahkan informasi tambahan di sini -->
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/be/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/jszip/dist/jszip.min.js') }}"></script>

    <script>
        $('#table').DataTable({
            responsive: true,
            dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [{
                    extend: 'csv',
                    className: 'btn-sm'
                },
                {
                    extend: 'excel',
                    className: 'btn-sm'
                },

            ],
        });
    </script>
@endpush
