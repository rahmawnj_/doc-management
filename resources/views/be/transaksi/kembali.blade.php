@extends('layouts.backend.app')
@php
    $title = 'Kembalian';
@endphp

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <!-- Form Sticky di Kiri -->
                    <form method="POST" action="{{ route('return_documents') }}">
                        @csrf
                        <div class="form-group">
                            <label for="inputText">QRCode</label>
                            <input type="text" class="form-control" name="qrcode" id="inputText">
                            <div id="qrcodeNotFound" style="color: red;"></div> <!-- Pesan jika qrcode tidak ditemukan -->
                        </div>
                        <!-- Add more QRCode inputs here if needed -->
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
                <div class="col-md-8">
                    <!-- Tabel Scrollable di Kanan -->
                    <div style="overflow-x: auto;">

                        <table class="table table-striped" id="resultTable">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Nama Dokumen</th>
                                    <th>Peminjam</th>
                                    <th>Tanggal Pinjam</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transactionDetails as $transactiondetail)
                                    <tr>
                                        <td>{{ $transactiondetail->document->nomor }}</td>
                                        <td>{{ $transactiondetail->document->nama }}</td>
                                        <td>{{ $transactiondetail->transaction->nama }}</td>
                                        <td>{{ $transactiondetail->transaction->waktu }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <!-- Tombol di Bawah Tabel -->

        </div>
    </div>
@endsection

@push('scripts')
@endpush
