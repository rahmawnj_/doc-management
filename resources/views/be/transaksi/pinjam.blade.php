@extends('layouts.backend.app')
@php
    $title = 'Peminjaman';
@endphp

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <!-- Form Sticky di Kiri -->
                    <div class="form-group">
                        <label for="inputText">QRCode</label>
                        <input type="text" class="form-control" id="inputText">
                        <div id="qrcodeNotFound" style="color: red;"></div> <!-- Pesan jika qrcode tidak ditemukan -->
                    </div>


                </div>
                <div class="col-md-8">
                    <!-- Tabel Scrollable di Kanan -->
                    <div style="overflow-x: auto;">
                        <form action="{{ route('submit_documents') }}" method="POST">
                            <!-- Form utama untuk mengirim data -->
                            @csrf
                            <div class="form-group">
                                <label for="inputNama">Nama Peminjam</label>
                                <input type="text" class="form-control" name="inputNama" required
                                    onkeydown="if(event.key === 'Enter') event.preventDefault();">
                            </div>
                            <table class="table table-striped" id="resultTable">
                                <thead>
                                    <tr>
                                        <th>QR Code</th>
                                        <th>Nomor</th>
                                        <th>Nama Dokumen</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>


                                </tbody>
                            </table>
                            <button class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <!-- Tombol di Bawah Tabel -->

        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/be/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
    @if (session('success'))
        <script>
            swal({
                title: 'Success',
                text: 'Dokumen Berhasil Dipinjam',
                icon: 'success',
               
            });
        </script>
    @endif


    <script>
        document.addEventListener("DOMContentLoaded", function() {

            const inputText = document.getElementById("inputText");
            const qrcodeNotFound = document.getElementById("qrcodeNotFound");
            const resultTable = document.getElementById("resultTable").getElementsByTagName('tbody')[0];
            const submitButton = document.getElementById("submitButton");

            inputText.addEventListener("keyup", function(event) {
                if (event.key === "Enter") {
                    event.preventDefault();
                    sendDataToApi(inputText.value);
                    console.log(inputText.value);
                }
            });

            submitButton.addEventListener("click", function() {
                event.preventDefault();
                submitAllDocuments();
            });

            function sendDataToApi(value) {
                fetch("{{ route('code_document') }}", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            value: value
                        }),
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        if (data.message && data.message === "qrcode tidak ditemukan") {
                            qrcodeNotFound.innerText = "qrcode tidak ditemukan";
                        } else {
                            qrcodeNotFound.innerText = "";
                            if (isQRCodeAlreadyInTable(data.document.qrcode)) {
                                alert("QR code sudah terdaftar dalam tabel.");
                            } else {
                                const newRow = resultTable.insertRow(resultTable.rows.length);
                                const cell1 = newRow.insertCell(0);
                                const cell2 = newRow.insertCell(1);
                                const cell3 = newRow.insertCell(2);
                                const cell4 = newRow.insertCell(3);

                                cell1.innerHTML = data.document.qrcode;
                                cell2.innerHTML = data.document.nomor;
                                cell3.innerHTML = data.document.nama;

                                const qrCodeInput = document.createElement("input");
                                qrCodeInput.type = "hidden";
                                qrCodeInput.name =
                                    "qrcode[]"; // Menggunakan array untuk menyimpan nilai multiple qrcode
                                qrCodeInput.value = data.document.qrcode;

                                cell4.appendChild(qrCodeInput); // Menambahkan input hidden ke dalam cell ke-4


                                const trashButton = document.createElement("button");
                                const trashIcon = document.createElement("i");

                                trashIcon.classList.add("fas", "fa-trash");

                                trashButton.appendChild(trashIcon);
                                trashButton.classList.add("btn", "btn-danger", "btn-sm");
                                trashButton.addEventListener("click", function() {
                                    deleteTableRow(newRow);
                                });

                                cell4.appendChild(trashButton);

                                inputText.value = '';
                            }
                        }
                    })
                    .catch((error) => {
                        console.error("Error:", error);
                    });
            }

            function isQRCodeAlreadyInTable(qrCode) {
                const rows = resultTable.getElementsByTagName("tr");
                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];
                    const cells = row.getElementsByTagName("td");
                    if (cells.length > 0 && cells[0].innerText === qrCode) {
                        return true;
                    }
                }
                return false;
            }

            function deleteTableRow(row) {
                if (confirm("Apakah Anda yakin ingin menghapus baris ini?")) {
                    row.remove();
                }
            }

            function submitAllDocuments() {
                // Mengumpulkan QR code yang ada di dalam tabel
                const qrCodes = [];
                const rows = resultTable.getElementsByTagName("tr");
                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];
                    const cells = row.getElementsByTagName("td");
                    if (cells.length > 0) {
                        const qrCode = cells[0].innerText;
                        qrCodes.push(qrCode);
                    }
                }

                // Mengirim QR code ke controller dengan permintaan POST
                fetch("{{ route('submit_documents') }}", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            "X-CSRF-TOKEN": "{{ csrf_token() }}",
                        },
                        body: JSON.stringify({
                            qrCodes: qrCodes
                        }),
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        if (data.message && data.message === "Semua dokumen telah berhasil disubmit") {
                            alert("Semua dokumen telah berhasil disubmit.");
                            // Tambahkan kode lain yang diperlukan setelah sukses mengupdate dokumen
                        } else {
                            alert("Terjadi kesalahan saat mengupdate dokumen.");
                        }
                    })
                    .catch((error) => {
                        console.error("Error:", error);
                        alert("Terjadi kesalahan saat mengupdate dokumen.");
                    });
            }


        });


        // document.addEventListener("DOMContentLoaded", function() {
        //     const inputText = document.getElementById("inputText");
        //     const qrcodeNotFound = document.getElementById("qrcodeNotFound");
        //     const resultTable = document.getElementById("resultTable").getElementsByTagName('tbody')[0];

        //     inputText.addEventListener("keyup", function(event) {
        //         if (event.key === "Enter") {
        //             event
        //                 .preventDefault(); // Mencegah tindakan default form (misalnya, mengirimkan formulir)
        //             sendDataToApi(inputText.value); // Panggil fungsi untuk mengirim data ke API
        //             console.log(inputText.value);
        //         }
        //     });

        //     function sendDataToApi(value) {
        //         // Lakukan permintaan API di sini, Anda dapat menggunakan JavaScript fetch atau library lain
        //         // Contoh penggunaan fetch:
        //         fetch("{{ route('code_document') }}", {
        //                 method: "POST",
        //                 headers: {
        //                     "Content-Type": "application/json",
        //                 },
        //                 body: JSON.stringify({
        //                     value: value
        //                 }), // Kirim data ke API dalam format JSON
        //             })
        //             .then((response) => response.json())
        //             .then((data) => {
        //                 if (data.message && data.message === "qrcode tidak ditemukan") {
        //                     qrcodeNotFound.innerText = "qrcode tidak ditemukan";
        //                     // resultTable.innerHTML = ""; // Kosongkan tabel jika qrcode tidak ditemukan
        //                 } else {
        //                     qrcodeNotFound.innerText = ""; // Hapus pesan jika qrcode ditemukan

        //                     if (isQRCodeAlreadyInTable(data.document.nomor)) {
        //                         alert("QR code sudah terdaftar dalam tabel.");
        //                     } else {
        //                         const newRow = resultTable.insertRow(resultTable.rows.length);
        //                         const cell1 = newRow.insertCell(0);
        //                         const cell2 = newRow.insertCell(1);
        //                         const cell3 = newRow.insertCell(2);

        //                         cell1.innerHTML = data.document.qrcode;
        //                         cell1.innerHTML = data.document.nomor;
        //                         cell2.innerHTML = data.document.nama;



        //                         const trashButton = document.createElement("button");
        //                         const trashIcon = document.createElement("i");

        //                         trashIcon.classList.add("fas", "fa-trash");

        //                         trashButton.appendChild(trashIcon);
        //                         trashButton.classList.add("btn", "btn-danger", "btn-sm");
        //                         trashButton.addEventListener("click", function() {
        //                             deleteTableRow(newRow);
        //                         });

        //                         cell3.appendChild(trashButton);


        //                         inputText.value = '';
        //                     }
        //                 }
        //             })
        //             .catch((error) => {
        //                 console.error("Error:", error);
        //             });
        //     }

        //     function isQRCodeAlreadyInTable(qrCode) {
        //         const rows = resultTable.getElementsByTagName("tr");
        //         for (let i = 0; i < rows.length; i++) {
        //             const row = rows[i];
        //             const cells = row.getElementsByTagName("td");
        //             if (cells.length > 0 && cells[0].innerText === qrCode) {
        //                 return true;
        //             }
        //         }
        //         return false;
        //     }

        //     // Fungsi untuk menghapus baris tabel
        //     function deleteTableRow(row) {
        //         if (confirm("Apakah Anda yakin ingin menghapus baris ini?")) {
        //             row.remove();
        //         }
        //     }
        // });
    </script>
@endpush
