@extends('layouts.backend.app')
@php
    $title = 'Box';
@endphp

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    {{-- <h1 class="page-header">Blank Page <small>header small text goes here...</small></h1> --}}
    <!-- END page-header -->
    <!-- BEGIN panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('boxes.update', $box->id) }}" method="POST">
                @csrf
                @method('PUT')
                <fieldset>
                    <legend class="mb-3">Box Edit</legend>

                    {{-- <div class="mb-3">
                        <label class="form-label" for="rak_id">Rak</label>
                        <select class="form-control @error('rak_id') is-invalid @enderror" id="rak_id" name="rak_id">
                            <option value="">Pilih Rak</option>
                            @foreach ($raks as $rak)
                                <option value="{{ $rak->id }}"
                                    {{ old('rak_id', $box->rak_id) == $rak->id ? 'selected' : '' }}>
                                    {{ $rak->ruangan->nama }} - {{ $rak->kode_rak }}</option>
                            @endforeach
                        </select>
                        @error('rak_id')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div> --}}

                    <div class="mb-3">
                        <label class="form-label" for="nama_kardus">Nama Kardus</label>
                        <input class="form-control @error('nama_kardus') is-invalid @enderror" type="text"
                            id="nama_kardus" name="nama_kardus" placeholder="Enter nama kardus"
                            value="{{ old('nama_kardus', $box->nama_kardus) }}">
                        @error('nama_kardus')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </fieldset>
            </form>
        </div>
    </div>
@endsection
