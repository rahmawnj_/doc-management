@extends('layouts.backend.app')
@php
    $title = 'Check Document';
@endphp

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
        <li class="breadcrumb-item active">{{ $title ?? '' }}</li>
    </ol>
    <!-- END breadcrumb -->

    {{-- <div class="row"> --}}
    {{-- <div class="col-md-8"> --}}
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('checkDocument') }}" method="GET">
                <div class="mb-3">
                    <div class="row">
                        <div class="col-3">
                            <label for="qrCode" class="form-label">QR Code</label>
                            <input type="text" class="form-control" id="qrCode" name="qrCode" required>
                        </div>
                    </div>
                </div>
            </form>

            <hr>

            @if (isset($document))
                <div class="document-info">
                    <h1>{{ $document->nama }}</h1>
                    <div class="status">
                        <span class="badge {{ $document->completed === 0 ? 'badge-success' : 'badge-danger' }}">
                            {{ $document->completed === 0 ? 'Tersimpan' : 'Dipinjam' }}
                        </span>
                    </div>
                    <div class="row">
                        <h2>Informasi Dokumen:</h2>
                        <div class="col-md-6">
                            <div class="document-details">
                                <ul>
                                    <li><strong>Ruangan:</strong> {{ $document->ruangan ? $document->ruangan->nama : '-' }}</li>
                                    <li><strong>Rak:</strong> {{ $document->rak ? $document->rak->nama : '-' }}</li>
                                    <li><strong>Kardus:</strong> {{ $document->kardus ? $document->kardus->nama : '-' }}</li>
                                    <li><strong>Status: </strong><span class="badge bg-primary"><b>{{ $document->completed == 1 ? 'Dipinjam' : 'Tersimpan'  }}</b></span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="document-details">
                                <ul>
                                    <li><strong>Map:</strong> {{ $document->map ? $document->map->nama : '-' }}</li>
                                    <li><strong>Kategori:</strong> {{ $document->kategori ? $document->kategori->nama : '-' }}</li>
                                    <li><strong>Bidang:</strong> {{ $document->bidang ? $document->bidang->nama : '-' }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
            @elseif (isset($message))
                <div class="alert alert-danger">
                    Document not found.
                </div>
            @endif



        </div>
    </div>
    {{-- </div> --}}

    {{-- <div class="col-md-4">
            <div class="card">
                <!-- Konten kartu di sini -->
            </div>
        </div> --}}
    {{-- </div> --}}
@endsection
