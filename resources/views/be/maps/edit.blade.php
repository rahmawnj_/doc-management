@extends('layouts.backend.app')
@php
    $title = 'Ruangan';
@endphp
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    {{-- <h1 class="page-header">Blank Page <small>header small text goes here...</small></h1> --}}
    <!-- END page-header -->
    <!-- BEGIN panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('maps.update', $map->id) }}" method="POST">
                @csrf
                @method('PUT')
                <fieldset>
                    <legend class="mb-3">Map Details</legend>

                    <div class="mb-3">
                        <label class="form-label" for="nama_map">Nama Map</label>
                        <input class="form-control @error('nama_map') is-invalid @enderror" type="text" id="nama_map"
                            name="nama_map" placeholder="Enter nama map" value="{{ old('nama_map', $map->nama_map) }}">
                        @error('nama_map')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </fieldset>
            </form>
        </div>
    </div>
@endsection
