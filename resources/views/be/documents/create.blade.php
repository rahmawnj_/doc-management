@extends('layouts.backend.app')
@php
    $title = 'Dokumen';
@endphp

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    {{-- <h1 class="page-header">Blank Page <small>header small text goes here...</small></h1> --}}
    <!-- END page-header -->
    <!-- BEGIN panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="panel-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('documents.store') }}" method="POST">
                @csrf
                <fieldset>
                    <legend>Document Information</legend>

                    <!-- Nama -->
                    <div class="mb-3">
                        <label class="form-label" for="nama">Nama</label>
                        <input class="form-control @error('nama') is-invalid @enderror" type="text" id="nama"
                            name="nama" placeholder="Enter nama" value="{{ old('nama') }}">
                        @error('nama')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Nomor -->
                    <div class="mb-3">
                        <label class="form-label" for="nomor">Nomor</label>
                        <input class="form-control @error('nomor') is-invalid @enderror" type="text" id="nomor"
                            name="nomor" placeholder="Enter nomor" value="{{ old('nomor') }}">
                        @error('nomor')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Date Document -->
                    <div class="mb-3">
                        <label class="form-label" for="date_document">Date Document</label>
                        <input class="form-control @error('date_document') is-invalid @enderror" type="date"
                            id="date_document" name="date_document" value="{{ old('date_document') }}">
                        @error('date_document')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- QR Code -->
                    <div class="mb-3">
                        <label class="form-label" for="qrcode">QR Code</label>
                        <input class="form-control @error('qrcode') is-invalid @enderror" type="text" id="qrcode"
                            name="qrcode" placeholder="Enter QR code" value="{{ old('qrcode') }}">
                        @error('qrcode')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Uraian -->
                    <div class="mb-3">
                        <label class="form-label" for="uraian">Uraian</label>
                        <textarea class="form-control @error('uraian') is-invalid @enderror" id="uraian" name="uraian"
                            placeholder="Enter uraian">{{ old('uraian') }}</textarea>
                        @error('uraian')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- SPK -->
                    <div class="mb-3">
                        <label class="form-label" for="spk">SPK</label>
                        <input class="form-control @error('spk') is-invalid @enderror" type="text" id="spk"
                            name="spk" placeholder="Enter SPK" value="{{ old('spk') }}">
                        @error('spk')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Date SPK -->
                    <div class="mb-3">
                        <label class="form-label" for="date_spk">Date SPK</label>
                        <input class="form-control @error('date_spk') is-invalid @enderror" type="date" id="date_spk"
                            name="date_spk" value="{{ old('date_spk') }}">
                        @error('date_spk')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- RAB -->
                    <div class="mb-3">
                        <label class="form-label" for="rab">RAB</label>
                        <input class="form-control @error('rab') is-invalid @enderror" type="text" id="rab"
                            name="rab" placeholder="Enter RAB" value="{{ old('rab') }}">
                        @error('rab')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- BAP -->
                    <div class="mb-3">
                        <label class="form-label" for="bap">BAP</label>
                        <input class="form-control @error('bap') is-invalid @enderror" type="text" id="bap"
                            name="bap" placeholder="Enter BAP" value="{{ old('bap') }}">
                        @error('bap')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- BASTB -->
                    <div class="mb-3">
                        <label class="form-label" for="bastb">BASTB</label>
                        <input class="form-control @error('bastb') is-invalid @enderror" type="text" id="bastb"
                            name="bastb" placeholder="Enter BASTB" value="{{ old('bastb') }}">
                        @error('bastb')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- SP2D -->
                    <div class="mb-3">
                        <label class="form-label" for="sp2d">SP2D</label>
                        <input class="form-control @error('sp2d') is-invalid @enderror" type="text" id="sp2d"
                            name="sp2d" placeholder="Enter SP2D" value="{{ old('sp2d') }}">
                        @error('sp2d')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Date SP2D -->
                    <div class="mb-3">
                        <label class="form-label" for="date_sp2d">Date SP2D</label>
                        <input class="form-control @error('date_sp2d') is-invalid @enderror" type="date"
                            id="date_sp2d" name="date_sp2d" value="{{ old('date_sp2d') }}">
                        @error('date_sp2d')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- SPM -->
                    <div class="mb-3">
                        <label class="form-label" for="spm">SPM</label>
                        <input class="form-control @error('spm') is-invalid @enderror" type="text" id="spm"
                            name="spm" placeholder="Enter SPM" value="{{ old('spm') }}">
                        @error('spm')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- SPJ -->
                    <div class="mb-3">
                        <label class="form-label" for="spj">SPJ</label>
                        <input class="form-control @error('spj') is-invalid @enderror" type="text" id="spj"
                            name="spj" placeholder="Enter SPJ" value="{{ old('spj') }}">
                        @error('spj')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Nilai -->
                    <div class="mb-3">
                        <label class="form-label" for="nilai">Nilai</label>
                        <input class="form-control @error('nilai') is-invalid @enderror" type="text" id="nilai"
                            name="nilai" placeholder="Enter Nilai" value="{{ old('nilai') }}">
                        @error('nilai')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- PT -->
                    <div class="mb-3">
                        <label class="form-label" for="pt">PT</label>
                        <input class="form-control @error('pt') is-invalid @enderror" type="text" id="pt"
                            name="pt" placeholder="Enter PT" value="{{ old('pt') }}">
                        @error('pt')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Completed -->
                    {{-- <div class="mb-3">
                        <label class="form-check-label" for="completed">Completed</label>
                        <input class="form-check-input" type="checkbox" id="completed" name="completed"
                            {{ old('completed') ? 'checked' : '' }}>
                    </div> --}}

                    <!-- ID Ruangan -->
                    <div class="mb-3">
                        <label class="form-label" for="id_ruangan">ID Ruangan</label>
                        <select class="form-control @error('id_ruangan') is-invalid @enderror" id="id_ruangan"
                            name="id_ruangan">
                            <option value="">Pilih Ruangan</option>
                            @foreach ($ruangans as $ruangan)
                                <option value="{{ $ruangan->id }}"
                                    {{ old('id_ruangan') == $ruangan->id ? 'selected' : '' }}>
                                    {{ $ruangan->nama }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_ruangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID Rak -->
                    <div class="mb-3">
                        <label class="form-label" for="id_rak">ID Rak</label>
                        <select class="form-control @error('id_rak') is-invalid @enderror" id="id_rak"
                            name="id_rak">
                            <option value="">Pilih Rak</option>
                            @foreach ($raks as $rak)
                                <option value="{{ $rak->id }}" {{ old('id_rak') == $rak->id ? 'selected' : '' }}>
                                    {{ $rak->kode_rak }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_rak')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID Box -->
                    <div class="mb-3">
                        <label class="form-label" for="id_box">ID Box</label>
                        <select class="form-control @error('id_box') is-invalid @enderror" id="id_box"
                            name="id_box">
                            <option value="">Pilih Box</option>
                            @foreach ($boxes as $box)
                                <option value="{{ $box->id }}" {{ old('id_box') == $box->id ? 'selected' : '' }}>
                                    {{ $box->nama_kardus }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_box')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID Map -->
                    <div class="mb-3">
                        <label class="form-label" for="id_map">ID Map</label>
                        <select class="form-control @error('id_map') is-invalid @enderror" id="id_map"
                            name="id_map">
                            <option value="">Pilih Map</option>
                            @foreach ($maps as $map)
                                <option value="{{ $map->id }}" {{ old('id_map') == $map->id ? 'selected' : '' }}>
                                    {{ $map->nama_map }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_map')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID Category -->
                    <div class="mb-3">
                        <label class="form-label" for="id_category">ID Category</label>
                        <select class="form-control @error('id_category') is-invalid @enderror" id="id_category"
                            name="id_category">
                            <option value="">Pilih Category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ old('id_category') == $category->id ? 'selected' : '' }}>
                                    {{ $category->nama }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_category')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID Bidang -->
                    <div class="mb-3">
                        <label class="form-label" for="id_bidang">ID Bidang</label>
                        <select class="form-control @error('id_bidang') is-invalid @enderror" id="id_bidang"
                            name="id_bidang">
                            <option value="">Pilih Bidang</option>
                            @foreach ($bidangs as $bidang)
                                <option value="{{ $bidang->id }}"
                                    {{ old('id_bidang') == $bidang->id ? 'selected' : '' }}>
                                    {{ $bidang->nama }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_bidang')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID User -->
                    <div class="mb-3">
                        <label class="form-label" for="id_user">ID User</label>
                        <select class="form-control @error('id_user') is-invalid @enderror" id="id_user"
                            name="id_user">
                            <option value="">Pilih User</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ old('id_user') == $user->id ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_user')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- ID SKPD -->
                    <div class="mb-3">
                        <label class="form-label" for="id_skpd">ID SKPD</label>
                        <select class="form-control @error('id_skpd') is-invalid @enderror" id="id_skpd"
                            name="id_skpd">
                            <option value="">Pilih SKPD</option>
                            @foreach ($skpds as $skpd)
                                <option value="{{ $skpd->id }}" {{ old('id_skpd') == $skpd->id ? 'selected' : '' }}>
                                    {{ $skpd->nama }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_skpd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Create</button>
                </fieldset>
            </form>
        </div>
    </div>
@endsection
