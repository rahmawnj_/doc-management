@extends('layouts.backend.app')

@php
    $title = 'Dokumen';
@endphp

@push('styles')
    <link href="{{ asset('assets/be/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/be/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}"
        rel="stylesheet" />
    <link href="{{ asset('assets/be/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}"
        rel="stylesheet" />
    <link href="{{ asset('assets/be/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/be/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/be/plugins/switchery/dist/switchery.min.js') }}"></script>
@endpush
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ $title ?? '' }}</a></li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    <h1 class="page-header">{{ $title ?? '' }}</h1>
    <!-- END page-header -->
    <!-- BEGIN panel -->
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i
                        class="fa fa-minus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i
                        class="fa fa-times"></i></a>
                <a href="{{ route('documents.create') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <table id="table" class="table table-striped table-bordered align-middle">
                <thead>
                    <tr>
                        <th width="1%">No.</th>
                        <th class="text-nowrap">Nama</th>
                        <th class="text-nowrap">Nomor</th>
                        <th class="text-nowrap">Date Document</th>
                        <th class="text-nowrap">Completed</th>
                        <th class="text-nowrap">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($documents as $document)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $document->nama }}</td>
                            <td>{{ $document->nomor }}</td>
                            <td>{{ $document->date_document }}</td>
                            <td>

                                <form action="{{ route('updateCompeleted', $document->id) }}" method="POST"
                                    id="update-form">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <input type="checkbox" id="completed-{{ $document->id }}" name="completed"
                                            class="toggle-completed" data-document-id="{{ $document->id }}"
                                            {{ $document->completed ? 'checked' : '' }}>

                                        {{-- <input type="checkbox" id="switchery-{{ $document->id }}" class="switchery"
                                            data-id="{{ $document->id }}"
                                            {{ $document->completed == 1 ? 'checked' : '' }} /> --}}
                                    </div>
                                </form>

                            </td>

                            <td>
                                <a href="#modal-dialog-{{ $document->id }}" class="btn btn-primary"
                                    data-bs-toggle="modal"><i class="fa fa-pencil"></i></a>

                                    <div class="modal fade" id="modal-dialog-{{ $document->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Modal Dialog</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab"
                                                                aria-controls="tab1" aria-selected="true">Tab 1</a>
                                                        </li>
                                                        <li class="nav-item" role="presentation">
                                                            <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab"
                                                                aria-controls="tab2" aria-selected="false">Tab 2</a>
                                                        </li>
                                                        <!-- Add more tabs as needed -->
                                                    </ul>
                                                    <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                                                            <!-- Tab 1 Form Fields -->
                                                            <!-- Nama -->
                                                            <!-- ... Other Fields ... -->
                                                        </div>
                                                        <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                                                            <!-- Tab 2 Form Fields -->
                                                            <!-- SPK -->
                                                            <!-- ... Other Fields ... -->
                                                        </div>
                                                        <!-- Add more tab content as needed -->
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="javascript:;" class="btn btn-white" data-bs-dismiss="modal">Close</a>
                                                    <a href="javascript:;" class="btn btn-success">Action</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    

                                <a href="{{ route('documents.edit', $document->id) }}" class="btn btn-warning btn-sm">
                                    <i class="fas fa-edit"></i> Edit
                                </a>
                                <form action="{{ route('documents.destroy', $document->id) }}" method="POST"
                                    style="display: inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah Anda yakin ingin menghapus dokumen ini?')">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/be/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/be/plugins/jszip/dist/jszip.min.js') }}"></script>

    <script src="{{ asset('assets/be/plugins/gritter/js/jquery.gritter.min.js') }}"></script>

    <script>
        var switcheryElements = document.querySelectorAll('.toggle-completed');
        Array.prototype.forEach.call(switcheryElements, function(el) {
            var switchery = new Switchery(el, {
                color: '#00acac'
            });
        });
    </script>

    <script>
        $('#table').DataTable({
            responsive: true,
            dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [{
                    extend: 'csv',
                    className: 'btn-sm'
                },
                {
                    extend: 'excel',
                    className: 'btn-sm'
                },

            ],
        });
    </script>
    <script>
        // Tangkap elemen checkbox
        const toggleCompleted = document.querySelector('.toggle-completed');

        // Tambahkan event listener untuk perubahan pada checkbox
        toggleCompleted.addEventListener('change', function() {
            // Simpan perubahan secara otomatis saat checkbox berubah
            document.getElementById('update-form').submit();
        });
    </script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const toggleCompleted = document.querySelectorAll(".toggle-completed");

            toggleCompleted.forEach((toggle) => {
                toggle.addEventListener("change", function() {
                    const documentId = this.getAttribute("data-document-id");
                    const completed = this.checked;
                    var url = '{{ route('updateCompeleted', ':documentId') }}';
                    url = url.replace(':documentId', documentId);

                    fetch(url, {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/json",
                                "X-CSRF-TOKEN": "{!! csrf_token() !!}",
                            },
                            body: JSON.stringify({
                                completed: completed
                            }),
                        })
                        .then((response) => response.json())
                        .then((data) => {
                            if (data.success) {
                                $.gritter.add({
                                    title: 'Success!',
                                    text: 'Dokumen berhasil diupdate',
                                    image: 'https://www.emmegi.co.uk/wp-content/uploads/2019/01/User-Icon.jpg',
                                    sticky: true,
                                    time: '',
                                    class_name: 'my-sticky-class gritter-light',
                                    before_open: function() {},
                                    after_open: function(e) {},
                                    before_close: function(manual_close) {},
                                    after_close: function(manual_close) {}
                                });

                                // $.gritter.removeAll({
                                //     before_close: function(e) {},
                                //     after_close: function() {}
                                // });
                            } else {
                                $.gritter.add({
                                    title: 'Error!',
                                    text: 'Dokumen gagal diupdate',
                                    image: 'https://www.emmegi.co.uk/wp-content/uploads/2019/01/User-Icon.jpg',
                                    sticky: true,
                                    time: '',
                                    class_name: 'my-sticky-class gritter-light',
                                    before_open: function() {},
                                    after_open: function(e) {},
                                    before_close: function(manual_close) {},
                                    after_close: function(manual_close) {}
                                });


                            }

                        })
                        .catch((error) => {
                            console.error("Error:", error);
                        });
                });
            });
        });
    </script>
@endpush
