<!-- BEGIN #sidebar -->
<div id="sidebar" style="background-color: white;" class="app-sidebar">
    <!-- BEGIN scrollbar -->
    <div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
        <!-- BEGIN menu -->
        <div class="menu">
            <div class="menu-profile">
                <a href="javascript:;" class="menu-profile-link" data-toggle="app-sidebar-profile"
                    data-target="#appSidebarProfileMenu">
                    <div class="menu-profile-cover with-shadow"></div>
                    <div class="menu-profile-image">
                        <img src="{{ asset('storage/' . Auth::user()->image) }}" alt="" />
                    </div>
                    <div class="menu-profile-info">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1">
                                {{ Auth::user()->name }}
                            </div>
                            <div class="menu-caret ms-auto"></div>
                        </div>
                        {{-- <small>Front end developer</small> --}}
                    </div>
                </a>
            </div>
            <div id="appSidebarProfileMenu" class="collapse">
                <div class="menu-item pt-5px">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-cog"></i></div>
                        <div class="menu-text">Settings</div>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-pencil-alt"></i></div>
                        <div class="menu-text"> Send Feedback</div>
                    </a>
                </div>
                <div class="menu-item pb-5px">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-question-circle"></i></div>
                        <div class="menu-text"> Helps</div>
                    </a>
                </div>
                <div class="menu-divider m-0"></div>
            </div>
            <div class="menu-header">Navigation</div>

            <div class="menu-item {{ isset($title) && $title === 'Dashboard' ? ' active' : '' }}">
                <a href="boxes.html" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-tachometer-alt"></i>
                    </div>
                    <div class="menu-text">Dashboard</div>
                </a>
            </div>
            <div class="menu-item {{ isset($title) && $title == 'Check Document' ? ' active' : '' }}">
                <a href="{{ route('checkDocument') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-qrcode"></i>
                    </div>
                    <div class="menu-text">Check Document</div>
                </a>
            </div>

            <div class="menu-item {{ isset($title) && $title == 'Peminjaman' ? ' active' : '' }}">
                <a href="{{ route('pinjam') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-shopping-cart"></i> <!-- Ganti dengan ikon yang sesuai -->
                    </div>
                    <div class="menu-text">Peminjaman</div>
                </a>
            </div>
            <div class="menu-item {{ isset($title) && $title == 'Kembalian' ? ' active' : '' }}">
                <a href="{{ route('kembali') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-undo-alt"></i> <!-- Ganti dengan ikon yang sesuai -->
                    </div>
                    <div class="menu-text">Kembalian</div>
                </a>
            </div>
            <div class="menu-header">Data Master</div>
            <div class="menu-item {{ isset($title) && $title == 'User' ? ' active' : '' }}">
                <a href="{{ route('users.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="menu-text">User</div>
                </a>
            </div>
            <div class="menu-item {{ isset($title) && $title == 'Ruangan' ? ' active' : '' }}">
                <a href="{{ route('ruangans.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-door-open"></i>
                    </div>
                    <div class="menu-text">Ruangan</div>
                </a>
            </div>

            <div class="menu-item {{ isset($title) && $title == 'Rak' ? ' active' : '' }}">
                <a href="{{ route('raks.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-book"></i>
                    </div>
                    <div class="menu-text">Rak</div>
                </a>
            </div>

            <div class="menu-item {{ isset($title) && $title == 'Box' ? ' active' : '' }}">
                <a href="{{ route('boxes.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="menu-text">Box</div>
                </a>
            </div>
            
            <div class="menu-item {{ isset($title) && $title == 'Map' ? ' active' : '' }}">
                <a href="{{ route('maps.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-map"></i>
                    </div>
                    <div class="menu-text">Map</div>
                </a>
            </div>
            <div class="menu-item {{ isset($title) && $title == 'Kategori' ? ' active' : '' }}">
                <a href="{{ route('categories.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-list-alt"></i>
                    </div>
                    <div class="menu-text">Categories</div>
                </a>
            </div>
            <div class="menu-item {{ isset($title) && $title == 'Bidang' ? ' active' : '' }}">
                <a href="{{ route('bidangs.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-sitemap"></i>
                    </div>
                    <div class="menu-text">Bidang</div>
                </a>
            </div>
            <div class="menu-item {{ isset($title) && $title == 'SKPD' ? ' active' : '' }}">
                <a href="{{ route('skpds.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-building"></i>
                    </div>
                    <div class="menu-text">SKPD</div>
                </a>
            </div>

            <div class="menu-item {{ isset($title) && $title == 'Dokumen' ? ' active' : '' }}">
                <a href="{{ route('documents.index') }}" class="menu-link">
                    <div class="menu-icon">
                        <i class="fas fa-file"></i>
                    </div>
                    <div class="menu-text">Document</div>
                </a>
            </div>

            <div class="menu-header">Transaksi</div>

            <div class="menu-item {{ isset($title) && $title == 'Data Peminjaman' ? ' active' : '' }}">
                <a href="{{ route('transactions') }}" class="menu-link"> <!-- Pastikan route sesuai -->
                    <div class="menu-icon">
                        <i class="fas fa-list-alt"></i> <!-- Ganti dengan ikon yang sesuai -->
                    </div>
                    <div class="menu-text">Data Peminjaman</div>
                </a>
            </div>



            <!-- BEGIN minify-button -->
            <div class="menu-item d-flex">
                <a href="javascript:;" class="app-sidebar-minify-btn ms-auto" data-toggle="app-sidebar-minify"><i
                        class="fa fa-angle-double-left"></i></a>
            </div>
            <!-- END minify-button -->
        </div>
        <!-- END menu -->
    </div>
    <!-- END scrollbar -->
</div>
<div class="app-sidebar-bg"></div>
<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile"
        class="stretched-link"></a></div>
<!-- END #sidebar -->
