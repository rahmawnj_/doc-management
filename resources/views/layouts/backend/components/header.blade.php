<div id="header" class="app-header app-header-inverse">
    <!-- BEGIN navbar-header -->
    <div class="navbar-header">
        <a href="index.html" class="navbar-brand"><i class="fa fa-file fa-lg"></i>
            <b>{{ config('app.name', 'Doc Management') }}</b></a>
        <button type="button" class="navbar-mobile-toggler" data-toggle="app-sidebar-mobile">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- END navbar-header -->
    <!-- BEGIN header-nav -->
    <div class="navbar-nav">
        <div class="navbar-item navbar-form">
          
        </div>
      
        <div class="navbar-item navbar-user dropdown">
            <a href="#" class="navbar-link dropdown-toggle d-flex align-items-center" data-bs-toggle="dropdown">
                <img src="{{ asset('storage/' . Auth::user()->image) }}" alt="" />
                <span class="d-none d-md-inline">{{ Auth::user()->name }}</span> <b class="caret ms-6px"></b>
            </a>
            <div class="dropdown-menu dropdown-menu-end me-1">
                <a href="javascript:;" class="dropdown-item">Edit Profile</a>
                <a href="javascript:;" class="dropdown-item"><span
                        class="badge bg-danger float-end rounded-pill">2</span> Inbox</a>
                <a href="javascript:;" class="dropdown-item">Calendar</a>
                <a href="javascript:;" class="dropdown-item">Setting</a>
                <div class="dropdown-divider"></div>
                <a href="javascript:;" class="dropdown-item">Log Out</a>
            </div>
        </div>
    </div>
    <!-- END header-nav -->
</div>
