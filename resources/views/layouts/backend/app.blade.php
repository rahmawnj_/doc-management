<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Doc Management') }} | {{ $title ?? '' }}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN core-css ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('assets/be/css/vendor.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/be/css/facebook/app.min.css') }}" rel="stylesheet" />
    <!-- ================== END core-css ================== -->
    @stack('styles')
</head>

<body>
    <!-- BEGIN #loader -->
    <div id="loader" class="app-loader">
        <span class="spinner"></span>
    </div>
    <!-- END #loader -->

    <!-- BEGIN #app -->
    <div id="app" class="app app-header-fixed app-sidebar-fixed">
        <!-- BEGIN #header -->
        @include('layouts.backend.components.header')
        @include('layouts.backend.components.sidebar')
        <!-- BEGIN #content -->
        <div id="content" class="app-content">
            <!-- BEGIN breadcrumb -->
           
            @yield('content')

            <!-- END panel -->
        </div>

        <!-- BEGIN scroll-top-btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top"
            data-toggle="scroll-to-top"><i class="fa fa-angle-up"></i></a>
        <!-- END scroll-top-btn -->
    </div>
    <!-- END #app -->
    
    <!-- ================== BEGIN core-js ================== -->
    <script src="{{ asset('assets/be/js/vendor.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/app.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/theme/facebook.min.js') }}"></script>
    <!-- ================== END core-js ================== -->
    @stack('scripts')
</body>

</html>
