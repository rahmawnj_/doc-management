<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Doc Management') }} | {{ $title ?? '' }}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{ asset('assets/be/css/vendor.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/be/css/facebook/app.min.css') }}" rel="stylesheet" />
    <!-- ================== END core-css ================== -->
    @stack('styles')
</head>

<body>
    <div id="app" class="app app-header-fixed app-sidebar-fixed">
        {{-- <div id="content" class="app-content"> --}}
            @yield('content')
        {{-- </div> --}}
    </div>
    <script src="{{ asset('assets/be/js/vendor.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/app.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/theme/facebook.min.js') }}"></script>
    @stack('scripts')
</body>

</html>
