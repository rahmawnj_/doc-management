<?php

use App\Http\Controllers\BidangController;
use App\Http\Controllers\BoxController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\RakController;
use App\Http\Controllers\RuanganController;
use App\Http\Controllers\SkpdController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('ruangans', RuanganController::class);
    Route::resource('raks', RakController::class);
    Route::resource('boxes', BoxController::class);
    Route::resource('maps', MapController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('bidangs', BidangController::class);
    Route::resource('skpds', SkpdController::class);
    Route::resource('documents', DocumentController::class);
    Route::resource('users', UserController::class);

    Route::get('/check-document', [DocumentController::class, 'showCheckDocumentForm'])->name('checkDocument');
    Route::put('documents/{document}/update-completed', [DocumentController::class, 'updateCompleted'])->name('updateCompeleted');

    Route::get('pinjam', [TransactionController::class, 'pinjam'])->name('pinjam');
    Route::get('kembali', [TransactionController::class, 'kembali'])->name('kembali');
    Route::get('transactions', [TransactionController::class, 'index'])->name('transactions');
    Route::post('submit_documents', [TransactionController::class, 'submitDocuments'])->name('submit_documents');
    Route::post('return_documents', [TransactionController::class, 'returnDocuments'])->name('return_documents');
});
